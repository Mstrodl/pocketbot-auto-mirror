/* ----------------------------------------
	This file controls all the events fired
	off by presence changes from users, i.e.
	status, "Now playing...", and streams
 ---------------------------------------- */
var exports = module.exports = {};

let logger  = require("./logger"),
	dio		= require("./dio"),
	x 		= require("./vars"),
	req 	= require("request-promise-native"),
	TOKEN 	= require("../core/tokens"),
	helper	= require("./helpers");

exports.onChange = async function(status, userdata=null) {
	const fromID = status.userID,
		game = status.game,
		timer = new Date().getTime(),
		mem = status.bot.servers[x.chan].members[fromID],
		fromRoles = (mem && mem.hasOwnProperty("roles")) ? mem.roles : null;

	// Someone goes offline
	if (status.state === "offline") {
		// Check if they are on ready list
		if (fromRoles && fromRoles.includes(x.lfg)) {
			status.bot.removeFromRole({
				serverID: x.chan,
				userID: fromID,
				roleID: x.lfg
			}, function(err, resp) {
				if (err) {
					logger.log(`${err} | ${resp}`, "Error");
					return false;
				}
			});
		}

		// Copy user to FB
		userdata.setProp({ user: mem, prop: { name: "state" } });

		// // Save timestamp for dev
		if (fromRoles && fromRoles.includes(x.admin) ) userdata.setProp({ 
			user: fromID, 
			prop: { 
				name: "onlast", 
				data: timer 
			}
		});
	} else if (status.state === "online") {

		// Dev greetings
		if (fromRoles && fromRoles.includes(x.admin) ) {

			userdata.getProp(fromID, "onlast").then( (lastTime) => {
				if (lastTime && timer > (lastTime + 10800000)) { // Been more than 3 hours since offline
					const nickname = helper.getNickFromId(fromID, status.bot),
						greets = [
							`Greetings Master ${nickname}`,
							`o/ ${nickname}`,
							`May your devness shine light upon us all, ${nickname}`,
							`One ${nickname} a day makes bugs go away. Welcome back!`,
							`It's Butters, not Butter, ${nickname}!`,
							`Sup ${nickname}`,
							`${nickname} has entered the building.`,
							`${nickname} has joined the fight!`,
							`Aww yiss, ${nickname} in da house!`,
							`Hey ${nickname}, ready to start the bug-fixing party?`
						];

					if (fromID === x.schatz) {
						greets.push(
							`How's the fam, Master Schatz? ${x.emojis.schatz}`,
							`Ah! welcome back, Master Schatz! ${x.emojis.schatz}`,
							`Good of you to join us, Master Schatz. ${x.emojis.schatz}`,
							`${x.emojis.schatz} Master Schatz, you've returned.`
						);
					} else if (fromID === x.adam) {
						greets.push(
							`The pixel artist has returned. Welcome back Master Adam ${x.emojis.adam}.`,
							`Good of you to join us, Master Adam ${x.emojis.adam}.`,
							`${x.emojis.adam} Master DeGrandis, you've returned.`
						);
					} else if (fromID === x.stealth) {
						greets.push(
							`Ah! welcome back, Webmaster Stealth! ${x.emojis.masta}`,
							`Good of you to join us, Master Masta. ${x.emojis.masta}`,
							`Greetings, Mastastealth! ${x.emojis.masta}`,
							`Oh look, a real dev! Heya ${nickname}`
						);
					}

					let n = Math.floor( Math.random()*greets.length );
					dio.say(greets[n], status, x.chan);

					userdata.setProp({ 
						user: fromID, 
						prop: { 
							name: "onlast", 
							data: false 
						}
					});
				}
			}).catch( (err) => {
				logger.log(err, "Error");
			});
		}
	}

	// Someone is playing/streaming (?) the game
	if (game) {
		let gameName = game.name.toLowerCase(),
			streamer = ( game.hasOwnProperty("url") ) ?  game.url.substr(game.url.lastIndexOf("/") + 1) : null;

		// Let's get some stream info, if it exists
		if (streamer) await req({
			url: "https://api.twitch.tv/kraken/streams/"+streamer,
			headers: { "Client-ID": TOKEN.TWITCHID }
		}).then( (body)=> {
			streamer = JSON.parse(body).stream;
		}).catch( (err) => {
			logger.log(err, "Error");
		});

		// Check for all known game names and stream stuff 
		if ( gameName.match(/tooth\s?(and|&)\s?tail/gi) || gameName.includes("tnt") ) {
			// Add rank (if playing ranked)
			if (game.state && game.state.startsWith("Ranked (")) {
				// Grab the number, if there is one, which there should...
				const rank = (game.state.match(/([0-9])\w+/gi)) ? game.state.match(/([0-9])\w+/gi)[0] : false;

				if (!rank) return false;
				userdata.setProp({ 
					user: fromID, 
					prop: { 
						name: "rank", 
						data: parseInt(rank) 
					}
				});
			}

			if (!helper.isHeroku() || (fromRoles && fromRoles.includes(x.lfg))) return false;

			// Add to LFG
			status.bot.addToRole({
				serverID: x.chan,
				userID: fromID,
				roleID: x.lfg
			}, (err, resp) => {
				if (err) {
					logger.log(`${err} | ${resp}`, "Error");
					return false;
				}

				// And if the user is not a Recruit OR Veteran
				if (fromRoles && !fromRoles.includes(x.noob) && !fromRoles.includes(x.member)) {
					// Make em recruit
					status.bot.addToRole({
						serverID: x.chan,
						userID: fromID,
						roleID: x.noob
					});
				}
			});
		} else {
			// If he's not playing/streaming it, and has LFG, remove
			if (fromRoles && fromRoles.includes(x.lfg)) {
				status.bot.removeFromRole({
					serverID: x.chan,
					userID: fromID,
					roleID: x.lfg
				});
			}
		}
	} else {
		// Or if he stopped playing/streaming, remove LFG
		if (fromRoles && fromRoles.includes(x.lfg)) {
			status.bot.removeFromRole({
				serverID: x.chan,
				userID: fromID,
				roleID: x.lfg
			});
		}
	}

	// Log all changes in debug mode
	//if ( helper.isDebug() ) logger.log(`User '${from}' is now ${status.state}`);
};
