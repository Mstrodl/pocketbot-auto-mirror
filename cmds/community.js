/* ----------------------------------------
	This file contains most of the general
	commands (formerly in Mastabot) that the
	public community uses.
 ---------------------------------------- */

let logger  = require("../core/logger"),
	command = require("../core/command").Command,
	dio = require("../core/dio"),
	//helpers = require("../core/helpers"),
	x 		= require("../core/vars");

let cmdAlpha = new command("community", "!alpha", "Gives information about joining the alpha", function(data) {
	dio.del(data.messageID, data);
	dio.say(`:pencil: Learn more about the alpha in the <#${x.rules}> channel. :thumbsup:`,data);
});

let cmdControls = new command("community", "!controls", "Display game controls", function(data) {
	dio.say(":video_game: These are the current controls: http://pocketwatchgames.com/presskit/Tooth%20and%20Tail/images/controllercscreen.png",data);
});

let cmdBugs = new command("community", "!bugs", "Link to the troubleshooting channel", function(data) {
	dio.say(":beetle: Report bugs here: <#134810918113509376>",data);
});

let cmdWiki = new command("community", "!wiki", "Link to Freakspot's TnT Wiki", function(data) {
	dio.say("Check out the wiki: http://toothandtailwiki.com/",data);
});

let cmdTourney = new command("community", "!tournament", "Link to the Clash of Comrades website", function(data) {
	dio.say("For Tournament and game info, VODs, and other community events, check out http://www.clashofcomrades.com",data);
});

let cmdRoadmap = new command("community", "!roadmap", "Links to public roadmap", function(data) {
	dio.del(data.messageID,data);
	dio.say(":map: View the public roadmap here: <https://trello.com/b/N8EzNonZ/tooth-and-tail-roadmap> \n\n If you wish to comment or add something (into the **Guest Requests** only), sign up through here: <https://trello.com/invite/b/N8EzNonZ/4888ec2b4b67b55825dea3ad939b5bc1/tooth-and-tail-roadmap>", data);
});

let cmdTeam = new command("community", "!team", "Aligns user to a faction.", async function(data) {
	dio.del(data.messageID, data);

	const chosenteam = data.args[1].toLowerCase();
	let finalTeam = "";

	const team = await data.userdata.getProp(data.userID, "team");

	if (team !== "none") {
		dio.say("🕑 Signups are now closed. Please join us for Season 3!", data); 
		return false;
	}

	if (!team || (team && chosenteam === "none")) {
		if (chosenteam === "") { 
			dio.say("🕑 The command is `!team color` where you swap out `color` for that of your faction.", data); 
			return false;
		}
		
		data.bot.addToRole({
			serverID: x.chan,
			userID: data.userID,
			roleID: x.pwfaction
		}, async (err, resp) => {
			if (err) {
				logger.log(`${err} | ${resp}`, "Error");
				dio.say("🕑 Failed to role.", data);
				return false;
			}

			// Now say something
			switch (chosenteam) {
			case "red":
			case "commoners":
			case "commonfolk":
			case "orange":
			case "hopper":
				finalTeam = "red";
				break;
			case "blue":
			case "longcoats":
			case "bellafide":
				finalTeam = "blue";
				break;
			case "green":
			case "ksr":
			case "military":
			case "qm":
			case "quartermaster":
				finalTeam = "green";
				break;
			case "yellow":
			case "pink":
			case "purple":
			case "wine":
			case "clergy":
			case "civilized":
			case "archimedes":
				finalTeam = "yellow";
				break;
			case "none":
			case "null":
			case "leave":
				finalTeam = null;
				break;
			default:
				dio.say("🕑 Not sure what team you tried to join. Syntax is `!team <color>`.", data);
			}
		
			try {
				await data.db.soldiers.child(data.userID).update({ team: finalTeam });
		
				switch (finalTeam) {
				case "red":
					dio.say(`${x.emojis.hopper} Welcome to the **Commonfolk** faction, <@${data.userID}>!`, data);
					break;
				case "blue":
					dio.say(`${x.emojis.bellafide} Welcome to the **Longcoats** faction, <@${data.userID}>!`, data);
					break;
				case "green":
					dio.say(`${x.emojis.qm} Welcome to the **KSR** faction, <@${data.userID}>!`, data);
					break;
				case "yellow":
					dio.say(`${x.emojis.archi} Welcome to the **Civilized** faction, <@${data.userID}>!`, data);
					break;
				case null:
					dio.say(`You have left your team, <@${data.userID}>.`, data);
					break;
				}
			} catch(e) {
				logger.log(e, "Error");
			}
		});
	} else {
		dio.say(`🕑 You already have joined **${team}**! If you want to swap, do a \`!team none\` first, then choose new team.`, data);
	}
});

let cmdStress = new command("community", "!stress", "Sets a player's stress level for Pocketworld", async function(data) {
	dio.del(data.messageID, data);
	const stress = data.args[1].toLowerCase();

	try {
		const laststress = await data.userdata.getProp(data.userID, "stress");
		const now = Date.now();
		console.log(now); // eslint-disable-line

		if (laststress && laststress.date > now - (1000*60*60*3)) { // 1s -> 1m -> 1hr -> 3hr
			dio.say("🕑 You cannot change your stress level within a 3H period.", data);
			return false;
		} 

		if (stress !== "low" && stress !== "med" && stress !== "high") {
			dio.say("🕑 Please choose one of three stress levels: `!stress low|med|high`", data);
			return false;
		} else if (stress === laststress.stress) {
			dio.say("🕑 That is the same stress level you had before, silly pants.", data);
			return false;
		} else {
			// Update junk
			setTimeout(async () => {
				await data.db.soldiers.child(data.userID).update({ 
					stress: {
						stress,
						date: now
					}
				});
			}, 1000*60*5);

			dio.say(`🕑 Stress level set to: \`${stress}\`. This will activate in 5 minutes, and you cannot change this for 3 hours.`, data);
		}
	} catch(e) {
		logger.log(e, "Error");
	}

});

let cmd8Ball = new command("community", "!8ball", "Asks the 8ball a question", function(data) {

	let x = Math.floor(Math.random()*(20)-1),
		answer = [
			"It is certain", "It is decidedly so", "Without a doubt", "Yes, definitely",
			"As I see it, yes", "Most likely", "Outlook good", "Yes",
			"Signs point to yes", "Reply hazy try again", "Ask again later", "Better not tell you now",
			"Cannot predict now", "Concentrate and ask again", "Don't count on it", "My reply is no",
			"My sources say no", "Outlook not so good", "Very doubtful"
		];

	dio.say(`:8ball: says _"${answer[x]}"_`,data);
});

let cmdRegion = new command("community", "!region", "Allows user to set region roles on themselves", function(data) {
	if (data.args[1]) {
		let region = data.args[1].toLowerCase();

		switch(region) {
		case "na":
			data.bot.addToRole({
				serverID: x.chan,
				userID: data.userID,
				roleID: x.na
			}, function(err,resp) {
				if (err) {
					logger.log(`${err} | ${resp}`, "Error");
					dio.say("🕑 Failed to role.",data);
					return false;
				}

				dio.say(`Successfully added to the ${region.toUpperCase()} role.`, data);
			});
			break;
		case "eu":
			data.bot.addToRole({
				serverID: x.chan,
				userID: data.userID,
				roleID: x.eu
			}, function(err,resp) {
				if (err) {
					logger.log(`${err} | ${resp}`, "Error");
					dio.say("🕑 Failed to role.",data);
					return false;
				}

				dio.say(`Successfully added to the ${region.toUpperCase()} role.`, data);
			});
			break;
		case "au":
			data.bot.addToRole({
				serverID: x.chan,
				userID: data.userID,
				roleID: x.au
			}, function(err,resp) {
				if (err) {
					logger.log(`${err} | ${resp}`, "Error");
					dio.say("🕑 Failed to role.",data);
					return false;
				}

				dio.say(`Successfully added to the ${region.toUpperCase()} role.`, data);
			});
			break;
		default:
			dio.say("🕑 You didn't specify a region. Please choose one of the following: `NA | EU | AU`",data);
			break;
		}
	} else {
		dio.say("🕑 You didn't specify a region. Please choose one of the following: `NA | EU | AU`",data);
	}
});

module.exports.commands = [
	cmdRoadmap, cmdTourney, cmdWiki, 
	cmdBugs, cmdControls, cmdAlpha, 
	cmdTeam, cmdStress, cmd8Ball, cmdRegion
];

function emoteCmd(data, arr, e=false, ext=".gif") {
	dio.del(data.messageID, data);
	let key = data.args[0].replace(":","").replace(":",""), // Can't do aliases in debug like this
		emote = (e) ? e : arr[key],
		uRoles = data.bot.servers[x.chan].members[data.userID].roles;

	// Mods/Admins just go
	if (uRoles.includes(x.mod) || uRoles.includes(x.admin)) {
		dio.sendImage(`./emoji/${emote}${ext}`, data); // This could be handled better but this gets the job done
		logger.log(`Uploaded ${emote}${ext}`, "OK");
	} else {
		// Check everyone else
		data.userdata.getProp(data.userID, "emotes").then( (res) => {
			if ( res && res.includes(emote) ) {
				dio.sendImage(`./emoji/${emote}${ext}`, data);
				logger.log(`Uploaded ${emote}${ext}`, "OK");
			} else {
				dio.say("🕑 You have not purchased this emote. You should check the `!shop` for more information." , data);
			}
		});
	}
}

for (let key in x.gifaliases) {
	module.exports.commands.push(new command("emoji", ":"+key+":", "Sends the animated "+x.aliases[key]+" emote", function(data) {
		emoteCmd(data, x.gifaliases);
	}));
}

for (let key in x.aliases) {
	module.exports.commands.push(new command("emoji", ":"+key+":", "Sends the "+x.aliases[key]+" emote!", function(data) {
		emoteCmd(data, x.aliases, false, ".png");
	}));
	module.exports.commands[module.exports.commands.length-1].triggerType = 0;
}

x.gifemotes.forEach(function(emote){
	module.exports.commands.push(new command("emoji", ":"+emote+":", "Sends the animated "+emote+" emote!", function(data) {
		emoteCmd(data, false, emote);
	}));
});

x.emotes.forEach(function(emote){
	module.exports.commands.push(new command("emoji", ":"+emote+":", "Sends the "+emote+" emote", function(data) {
		emoteCmd(data, false, emote, ".png");
	}));
});
